import { Component, OnInit, AfterContentInit, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { SinglePostComponent } from '../single-post/single-post.component';

@Component({
  selector: 'app-posts-container',
  templateUrl: './posts-container.component.html',
  styleUrls: ['./posts-container.component.css']
})
export class PostsContainerComponent implements OnInit, AfterContentInit {

  @ViewChild('postsContainer', { read: ViewContainerRef }) container;

  constructor(private _componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {
  }

  ngAfterContentInit() {
    const singlePostFactory = this._componentFactoryResolver.resolveComponentFactory(SinglePostComponent);
    this.container.createComponent(singlePostFactory);

    const singlePostReference = this.container.createComponent(singlePostFactory);
    singlePostReference.instance.postTitle = 'factory title';
  }
}
